mass_import_mediafiles.py
============

mass\_import\_mediafiles.py uploads a list of MediaFiles to ARLO.

Before uploading files, the file list is compared against the existing
files within the Project and this will skip any files that already exist.
This is useful for interrupted transfers, where the same list can be used
again and only the missing files are imported.

    usage: mass_import_mediafiles.py [-h] [-v] [-d] [-u USER] [-p PASSWORD] [-f]
                                     [--project PROJECT] [--library LIBRARY]
                                     files [files ...]
    
    positional arguments:
      files                 Files to Import
    
    optional arguments:
      -h, --help            show this help message and exit
      -v, --verbose         Verbose Output
      -d, --debug           Debug Output
      -u USER, --user USER  Username of the Login User
      -p PASSWORD, --password PASSWORD, --pass PASSWORD
                            Password of the Login User
      -f, --force           Don't Prompt For Confirmation
      --project PROJECT     Project Id
      --library LIBRARY     Library Id

### Usage Example

Interactive Use

    $ python mass_import_mediafiles.py file1.wav file2.wav

Scripted Use

    $ python mass_import_mediafiles.py -u tonyb -p <PASSWORD> --project 93 --library 69 -f file1.wav file2.wav

### Required Modules

Requirements are maintained in requirements.txt. 

To install with pip: 
`pip install -r requirements.txt`

### Uploading MP3s

This does support uploading MP3 files via the Upload and Convert feature
in ARLO. Note that this script does not expose conversion settings directly,
using the automatic settings.

Due to the MP3 conversion and subsequent import running asynchronously from
the upload, files may not be immediately available after this script finishes.
Check the Project Jobs for conversion / import status.

### License

The MIT License (MIT)

Copyright (c) 2015-2018 tonyborries

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

