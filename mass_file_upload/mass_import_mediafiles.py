#!/usr/bin/python

# The MIT License (MIT)
#
# Copyright (c) 2015-2017 tonyborries
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import logging
import re
import sys
import os
import argparse
import getpass
import requests


# raw_input in 2.x, input in 3.x
try: input = raw_input  # pylint: disable=multiple-statements, redefined-builtin
except NameError: pass  # pylint: disable=multiple-statements, redefined-builtin


#########
# Settings

ARLO_BASE_URL = "https://live.arloproject.com/"  # must have trailing slash

ARLO_LOGIN_URL      = ARLO_BASE_URL + "accounts/login/?next=/tools/userHome"
ARLO_UPLOAD_URL     = ARLO_BASE_URL + "tools/uploadAudioFile"
ARLO_UPLOAD_CONVERT_URL = ARLO_BASE_URL + "tools/uploadAudioFile2"
ARLO_ADMIN_OVERRIDE = ARLO_BASE_URL + "tools/loginAsUser"
ARLO_API_V0_URL     = ARLO_BASE_URL + "api/v0/"

VERIFY_SSL = True  # Whether to Verify or Ignore TLS Cert errors


class UploadException(Exception):
    pass


class MediaFileUploader(object):

    project_id = None
    library_id = None
    allow_interactive = True
    files = None  # List of files provided by user, validated into _files_to_upload

    _session = None
    _csrf_token = None

    _project_name = None
    _library_name = None
    _project_id_names = None  # cache of ID: name
    _library_id_names = None  # id as an int
    _existing_library_mediafiles = None
    _files_to_upload = None  # list of tuples of (File Object, alias)


    def __init__(self, project_id=None, library_id=None, files=None, allow_interactive=True):
        if project_id:
            self.project_id = int(project_id)
        if library_id:
            self.library_id = int(library_id)
        self.files = files
        self.allow_interactive = allow_interactive

    def login(self, user=None, password=None, admin_override_user=None):
        """
        Login to ARLO.

        Save a logged-in requests.Session (session) object and the CSRF
        token (csrf_token) needed for form POSTs.

        @param user Username for login
        @param password Password for login
        @param admin_override_user If provided, login first with the user
               provided user/password, then override to login with this new
               username (Admins only)
        @return True on Success, False or raise an Exception on error
        """

        # Get Credentials

        if not user:
            if self.allow_interactive:
                user = input("Username: ")
            if not user:
                raise UploadException("Username not provided")

        if not password:
            if self.allow_interactive:
                password = getpass.getpass("Password: ")
            if not password:
                raise UploadException("Password not provided")

        logging.info("Logging in to ARLO...")

        # Setup Session
        self._session = requests.Session()
        self._session.headers['User-Agent'] = 'Mass_Import_MediaFiles/0.1'
        self._session.headers['Referer'] = ARLO_BASE_URL\

        # Get CSRF Token
        logging.debug("Retrieving Login Form...")
        resp = self._session.get(ARLO_LOGIN_URL, verify=VERIFY_SSL)
        if resp.status_code != requests.codes.ok:
            raise UploadException("Could not retrieve Login Form: %s" % resp.status_code)

        match = re.search("name='csrfmiddlewaretoken' value='([^']*)", resp.text)
        if match is None:
            raise UploadException("Could not retrieve CSRF Token")
        self._csrf_token = match.group(1)

        logging.debug("Found CSRF Token: %s", self._csrf_token)

        # Login
        logging.debug("Login POST...")

        resp = self._session.post(
            ARLO_LOGIN_URL,
            {
                'csrfmiddlewaretoken': self._csrf_token,
                'username': user,
                'password': password,
            },
            verify=VERIFY_SSL
        )
        if resp.status_code != requests.codes.ok:
            raise UploadException("Could not Login - Status Code: %s" % resp.status_code)

        # Re-retrieve CSRF if it has changed
        resp = self._session.get(ARLO_UPLOAD_URL, verify=VERIFY_SSL)
        match = re.search("name='csrfmiddlewaretoken' value='([^']*)", resp.text)
        if match is None:
            raise UploadException("Could not retrieve CSRF Token")
        self._csrf_token = match.group(1)
        logging.debug("Found CSRF Token: %s", self._csrf_token)

        # Undocumented Admin Override Feature
        if admin_override_user:
            logging.debug("Performing Admin Override...")

            resp = self._session.post(
                ARLO_ADMIN_OVERRIDE,
                {
                    'csrfmiddlewaretoken': self._csrf_token,
                    'newUser': admin_override_user,
                },
                verify=VERIFY_SSL)
            if resp.status_code != requests.codes.ok:
                raise UploadException("Could not Login Override - Status: %s" % resp.status_code)


        # validate the login by checking for the Welcome string
        if re.search("Log out", resp.text) is None:
            raise UploadException("Failed Logging In - Check user/password")

        logging.info("Successfully Logged In!")

        return True


    def select_project_library(self):
        """
        Select the Project / Library to which we'll upload.

        If not already defined, prompt the user for the Project / Library to
        which we will upload the MediaFiles.

        @param args Namespace of the program's settings arguments (originally
                     parsed from command line)
        @return True on Success, False on any error
        """

        # get the upload form, which has a list of project / libraries
        resp = self._session.get(ARLO_UPLOAD_URL, verify=VERIFY_SSL)
        if resp.status_code != requests.codes.ok:
            raise UploadException("Could not Retrieve Upload Form: %s" % resp.status_code)

        # validate the login by checking for the Welcome string
        if re.search("Upload Media File</div>", resp.text) is None:
            raise UploadException("ERROR: Failed Retrieving Upload Form")

        logging.debug("Retrieved Form!")


        # Get the Projects
        match = re.search('<select [^>]*id="id_project"[^>]*>(.*?)</select>', resp.text, re.DOTALL)
        if match is None:
            raise UploadException("Could not retrieve Projects List")

        self._project_id_names = {}
        for option_id, option_name in re.findall('<option value="(.*?)".*>(.*?)</option>', match.group(1)):
            if option_id != '':
                self._project_id_names[int(option_id)] = option_name

        # Get the Libraries
        match = re.search('<select [^>]*id="id_library"[^>]*>(.*?)</select>', resp.text, re.DOTALL)
        if match is None:
            raise UploadException("Could not retrieve Libraries List")

        self._library_id_names = {}
        for option_id, option_name in re.findall('<option value="(.*?)".*>(.*?)</option>', match.group(1)):
            if option_id != '':
                self._library_id_names[int(option_id)] = option_name

        # User Projects Prompt
        if self.project_id is None:
            if not self.allow_interactive:
                raise UploadException("Project ID Not Provided")
            print("\n\nid \tName\n---\t-----")
            for project_id in sorted(self._project_id_names.keys()):
                print("%d\t%s" % (project_id, self._project_id_names[project_id]))
            print("")
            self.project_id = int(input("Select Project Id: "))

        # Validate Project Selection
        if self.project_id not in self._project_id_names:
            raise UploadException("Invalid Project Selection")
        self._project_name = self._project_id_names[self.project_id]

        # User Library Prompt
        if self.library_id is None:
            if not self.allow_interactive:
                raise UploadException("Library ID Not Provided")
            print("\n\nid \tName\n---\t-----")
            for library_id in sorted(self._library_id_names.keys()):
                print("%d\t%s" % (library_id, self._library_id_names[library_id]))
            print("")
            self.library_id = int(input("Select Library Id: "))

        # Validate Library Selection
        if self.library_id not in self._library_id_names:
            raise UploadException("Invalid Library Selection")
        self._library_name = self._library_id_names[self.library_id]

        return True

    def get_file_alias(self, fileObj):
        """
        Compute the "Alias" to set for the uploaded file. Currently,
        this will just be the file basename, but future functionality
        may allow for overriding this.

        @param fileObj An opened File Object
        @return Alias to use for a file.
        """
        return os.path.basename(fileObj.name)

    def getLibraryMediaFiles(self):
        """
        Retrieve a list of MediaFiles currently in the Library, using the API
        """
        logging.info("Retrieving Existing Files...")
        self._existing_library_mediafiles = []
        next_url = ARLO_API_V0_URL + 'library/{}/mediaFiles/'.format(self.library_id)
        while next_url is not None:
            resp = self._session.get(next_url, verify=VERIFY_SSL)
            logging.debug(resp.text)
            if resp.status_code != requests.codes.ok:
                raise UploadException("Failed Retrieving Existing MediaFiles (Status: %s)" % resp.status_code)
            data = resp.json()
            self._existing_library_mediafiles.extend(data['results'])
            next_url = data['next']

        return self._existing_library_mediafiles

    def get_files_to_upload(self):
        """
        Iterate over 'files', skip existing and build a list of files we are
        going to upload.
        @param files A list of opened File objects, presumably from argparse
        """

        self.getLibraryMediaFiles()
        self._files_to_upload = []

        for f in self.files:
            found_existing = False
            file_alias = self.get_file_alias(f)
            for ef in self._existing_library_mediafiles:
                filename = ef['file'].split('/')[-1]
                if f.name in [filename, ef['alias']]:
                    found_existing = True
                if file_alias in [filename, ef['alias']]:
                    found_existing = True
            if found_existing:
                logging.info("Found Existing File: '%s' - Skipping", f.name)
            else:
                self._files_to_upload.append((f, file_alias))

        return self._files_to_upload


    def prompt_to_proceed(self):
        """
        Display a summary of the files we are going to Upload, and
        prompt for confirmation to proceed.

        If 'force' was set, skip this.

        @param args Namespace of the program's settings arguments (originally
                    parsed from command line)
        @return True on Success, False on any error
        """

        if not self.allow_interactive:
            return True

        # Display File Summary
        print("\n\nFound %d Files to Upload" % len(self._files_to_upload))
        print("---------------------------")

        for f, file_alias in self._files_to_upload:
            print(file_alias + " ( " + f.name + " )")
        print("")

        # Display Project / Library Info
        print("Project: %d (%s)" % (self.project_id, self._project_name))
        print("Library: %d (%s)" % (self.library_id, self._library_name))
        print("")

        # Prompt
        try:
            _ = input("Press Enter To Continue (Ctrl-C to Abort)")
        except KeyboardInterrupt:
            print("\n")
            logging.info("Exiting...")
            sys.exit(1)

        return True


    def upload_files(self):
        """
        Upload the Files to ARLO. Note that this may return with partial success,
        some files having been skipped.


        @return True on Success, False if any errors encountered (may not return
                immediately on error, rather continue on with the next file).
        """

        logging.info("***  Starting File Upload  ***")

        numToUpload = len(self._files_to_upload)
        numProcessed = 0
        numOk = 0
        for f, file_alias in self._files_to_upload:
            logging.info("Uploading %s (%d of %d)...", file_alias, (numProcessed + 1), numToUpload)
            if re.search(r'.*\.[Ww][Aa][Vv]$', f.name):
                success = self._uploadFile(f, file_alias)
            elif re.search(r'.*\.[Mm][Pp]3$', f.name):
                success = self._uploadAndConvertFile(f, file_alias)
            else:
                logging.warning("Unknown File Type, Skipping: %s", f.name)
                success = False
            if success:
                numOk = numOk + 1
            numProcessed = numProcessed + 1

        logging.info("***  Upload Summary: %d of %d Files Uploaded Successfully", numOk, numProcessed)

        return (numProcessed == numOk)

    def _uploadFile(self, f, file_alias):
        """
        Upload an individual file, return True if Successful
        then we're giving up on the file.
        @param f Opened File Object
        @param file_alias String Alias of the file
        """

        try:
            resp = self._session.post(
                ARLO_UPLOAD_URL,
                data = {
                    'csrfmiddlewaretoken': self._csrf_token,
                    'project': self.project_id,
                    'library': self.library_id,
                    'alias': file_alias,
                },
                files = {
                    'file': f
                 },
                 verify=VERIFY_SSL
            )
        except requests.exceptions.ConnectionError:
            logging.exception("Encountered Connection error during upload (%s) - skipping file", f.name)
            return False

        if resp.status_code != requests.codes.ok:
            logging.error("Failed Uploading File (Status: %s) - %s", resp.status_code, f.name)
            logging.debug(resp.text)
            return False

        # Check that we properly got redirected to the MediaFile Page
        if re.search("Audio Files Actions</div>", resp.text) is None:
            logging.error("Failed Uploading File - " + f.name)
            return False

        return True

    def _uploadAndConvertFile(self, f, file_alias):
        """
        Upload an individual file using the upload and Convert method
        @param f Opened File Object
        @param file_alias String Alias of the file
        @return True if Successful
        """

        try:
            resp = self._session.post(
                ARLO_UPLOAD_CONVERT_URL,
                data = {
                    'csrfmiddlewaretoken': self._csrf_token,
                    'project': self.project_id,
                    'library': self.library_id,
                    'alias': file_alias,
                },
                files = {
                    'file': f
                 },
                 verify=VERIFY_SSL
            )
        except requests.exceptions.ConnectionError:
            logging.exception("Encountered Connection error during upload (%s) - skipping file", f.name)
            return False

        if resp.status_code != requests.codes.ok:
            logging.error("Failed Uploading File (Status: %s) - %s", resp.status_code, f.name)
            logging.debug(resp.text)
            return False

        # Check that we properly got redirected to the ViewJob Page
        if re.search("Job Details</div>", resp.text) is None:
            logging.debug(resp.text)
            logging.error("Failed Uploading File - " + f.name)
            return False

        return True


def main(argv):

    ###
    # Get Command Line Arguments

    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', help='Verbose Output', default=False, action='store_true')
    parser.add_argument('-d', '--debug', help="Debug Output", default=False, action='store_true')
    parser.add_argument('-u', '--user', dest='user', help="Username of the Login User")
    parser.add_argument('-p', '--password', '--pass', dest='password', help="Password of the Login User")
    parser.add_argument('-f', '--force', dest='force', help="Don't Prompt For Confirmation", action='store_true')
    parser.add_argument('--project', help="Project Id")
    parser.add_argument('--library', help="Library Id")
    # Undocumented Feature - Use Admin Rights to Login as This UserName to Upload
    parser.add_argument('--admin-override', help=argparse.SUPPRESS, default=None, required=False)
    parser.add_argument('files', help="Files to Import", type=argparse.FileType('r'), nargs='+')
    args = parser.parse_args()

    # Setup Logging
    log_level = logging.WARNING
    if args.verbose:
        log_level = logging.INFO
    if args.debug:
        log_level = logging.DEBUG
    logging.basicConfig(format='%(levelname)s:%(message)s', level=log_level)
    logging.debug("DEBUG MODE ENABLED")
    logging.debug(repr(args))

    # Build Uploader
    uploader = MediaFileUploader(
        project_id=args.project,
        library_id=args.library,
        files=args.files,
        allow_interactive=not args.force
    )

    # Login
    if not uploader.login(user=args.user, password=args.password):
        print("Failed Login")
        sys.exit(1)

    if not uploader.select_project_library():
        print("Failed Selecting Project and Library")
        sys.exit(1)

    files_to_upload = uploader.get_files_to_upload()
    if not files_to_upload:
        logging.info("No Files to Upload - Exiting")
        sys.exit()

    if not uploader.prompt_to_proceed():
        logging.error("Failed to get Confirmation")
        sys.exit(1)

    if uploader.upload_files():
        logging.info("Successfully Uploaded All Files")
    else:
        logging.warning("Some Errors encountered During Upload - Check Output!!!")
        sys.exit(1)


if __name__ == "__main__":
    main(sys.argv[1:])
