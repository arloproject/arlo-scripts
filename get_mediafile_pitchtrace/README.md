get_mediafile_pitchtrace.py
============

get\_mediafile\_pitchtrace.py computes the PitchTrace data for an audio file 
from ARLO and saves it so a CSV formatted file. 

    usage: get_mediafile_pitchtrace.py [-h] [-d] [-u USER] [-p PASSWORD]
                                       [--mediafile MEDIAFILE] [--project PROJECT]
                                       [--startTime STARTTIME] [--endTime ENDTIME]
                                       --numTimeFramesPerSecond
                                       NUMTIMEFRAMESPERSECOND --numFrequencyBands
                                       NUMFREQUENCYBANDS --dampingFactor
                                       DAMPINGFACTOR --minFrequency MINFREQUENCY
                                       --maxFrequency MAXFREQUENCY
                                       --pitchTraceType {MAX_ENERGY,FUNDAMENTAL}
                                       [--numSamplePoints NUMSAMPLEPOINTS]
                                       [--minCorrelation MINCORRELATION]
                                       [--entropyThreshold ENTROPYTHRESHOLD]
                                       [--pitchTraceStartFreq PITCHTRACESTARTFREQ]
                                       [--pitchTraceEndFreq PITCHTRACEENDFREQ]
                                       [--minEnergyThreshold MINENERGYTHRESHOLD]
                                       [--tolerance TOLERANCE]
                                       [--inverseFreqWeight INVERSEFREQWEIGHT]
                                       [--maxPathLengthPerTransition MAXPATHLENGTHPERTRANSITION]
                                       [--windowSize WINDOWSIZE]
                                       [--extendRangeFactor EXTENDRANGEFACTOR]



### Output

This script generates a CSV formatted file, with PitchTrace and Energy as 
columns, and a row for each computed frame, where the number of rows will 
be equal to NUMTIMEFRAMESPERSECOND times the duration of the file. The file 
contents consist of:

* A header row for the column titles, as well as a copy of the settings used to generate the data. 
* A row for each frame computed in the file

Example:

    fileTime,frequency,energy,,pitchTraceType: MAX_ENERGY,numTimeFramesPerSecond: 200,numFrequencyBands: 1024,dampingFactor: 0.01,spectraMinFrequency: 75,spectraMaxFrequency: 10000
    0.0,9624.59997946,0.00344115726258
    0.005,10000.0,0.00502938369147

Output files are automatically named 'PitchTrace-PITCHTRACETYPE-FILEID.csv'

### Usage Example

Interactive Use

    $ python get_mediafile_pitchtrace.py --endTime 1 --mediafile 223502 --numTimeFramesPerSecond 200 --numFrequencyBands 1024 --dampingFactor 0.01 --minFrequency 75 --maxFrequency 10000 --pitchTraceType MAX_ENERGY

Scripted Use

    $ python get_mediafile_pitchtrace.py -u tonyb -p <PASSWORD> --endTime 1 --mediafile 223502 --numTimeFramesPerSecond 200 --numFrequencyBands 1024 --dampingFactor 0.01 --minFrequency 75 --maxFrequency 10000 --pitchTraceType MAX_ENERGY

### Required Modules

Requirements are maintained in requirements.txt. 

To install with pip: 
`pip install -r requirements.txt`

### License

The MIT License (MIT)

Copyright (c) 2015 tonyborries

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

