#!/usr/bin/python

# The MIT License (MIT)
#
# Copyright (c) 2015 tonyborries
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import sys
import argparse
import getpass
import requests
import re


# raw_input in 2.x, input in 3.x
try: input = raw_input
except NameError: pass


#########
# Settings

ARLO_BASE_URL = "http://live.arloproject.com:81/"  # must have trailing slash

ARLO_LOGIN_URL  = ARLO_BASE_URL + "accounts/login/?next=/tools/userHome"
ARLO_API_V0_URL = ARLO_BASE_URL + "api/v0/"

VERIFY_SSL = True  # Whether to Verify or Ignore TLS Cert errors

class PitchTrace():

  ###
  # Login to ARLO.
  #
  # Save a logged-in requests.Session (session) object and the CSRF
  # token (csrf_token) needed for form POSTs.
  #
  # @param args Namespace of the program's settings arguments (originally
  #             parsed from command line)
  # @return True on Success, False on any error

  def login(self, args):

    ###
    # Get Credentials

    if args.user is None:
      args.user = input("Username: ")
    if args.password is None:
      args.password = getpass.getpass("Password: ")

    if args.debug:
      print("DEBUG: Logging in to ARLO...")

    ###
    # Setup Session

    args.session = requests.Session()
    args.session.headers['User-Agent'] = 'Get_MediaFile_PitchTrace/0.1'
    args.session.headers['Referer'] = ARLO_BASE_URL

    ###
    # Get CSRF Token

    if args.debug:
      print("DEBUG: Retrieving Login Form...")

    resp = args.session.get(ARLO_LOGIN_URL, verify=VERIFY_SSL)
    if resp.status_code != requests.codes.ok:
      print("ERROR: Could not retrieve Login Form: " + str(resp.status_code))
      sys.exit(1)


    match = re.search("name='csrfmiddlewaretoken' value='([^']*)", resp.text)
    if match is None:
      print("ERROR: Could not retrieve CSRF Token")
      sys.exit(1)
    args.csrf_token = match.group(1)

    if args.debug:
      print("DEBUG: Found CSRF Token: " + args.csrf_token)

    ###
    # Login

    if args.debug:
      print("DEBUG: Login POST...")

    resp = args.session.post(ARLO_LOGIN_URL, {
                'csrfmiddlewaretoken': args.csrf_token,
                'username': args.user,
                'password': args.password,
                },
                verify=VERIFY_SSL)
    if resp.status_code != requests.codes.ok:
      print("ERROR: Could not Login: " + str(resp.status_code))
      if args.debug:
        print resp.text
      sys.exit(1)

    # validate the login by checking for the Welcome string
    if re.search("Welcome, <strong>", resp.text) is None:
      print("ERROR: Failed Logging In - Check user/password")
      sys.exit(1)

    if args.debug:
      print("DEBUG: Successfully Logged In!")

    args.password = "Password Cleared From Memory"

    return True


  ### Lookup the duration (in seconds) of the MediaFile.
  #
  # @param args Namespace of the program's settings arguments (originally
  #             parsed from command line)
  # @param mediaFileId
  # @return Length of the MediaFile, in seconds.

  def get_mediafile_duration(self, args, mediaFileId):

    ###
    # Get the MediaFile Details

    url = ARLO_API_V0_URL + "mediaFile/{}/".format(mediaFileId)
    if args.debug:
      print("DEBUG: Calling URL: " + url)
    resp = args.session.get(url, verify=VERIFY_SSL)
    if resp.status_code != requests.codes.ok:
      print("ERROR: Could not Retrieve MediaFile Details: " + str(resp.status_code))
      sys.exit(1)

    # decode JSON
    mediafile_detail = resp.json()

    ###
    # find the duration in the metadata

    if args.debug:
      print("DEBUG: Searching MediaFile MetaData for durationInSeconds")

    mediafile_duration = None
    for datum in mediafile_detail['mediafilemetadata_set']:
      if args.debug:
        print ("DEBUG:   - " + datum['name'] + ": " + datum['value'])
      if datum['name'] == "durationInSeconds":
        mediafile_duration = float(datum['value'])

    if mediafile_duration is None:
      print("ERROR: Failed Retrieving MediaFile Duration")
      sys.exit(1)

    return mediafile_duration

  ### Retrieve a list of MediaFile IDs in a Project.
  #
  # @param args Namespace of the program's settings arguments (originally
  #             parsed from command line)
  # @param projectId Database ID of the Project to retrieve.
  # @return List of MediaFile IDs.

  def get_project_mediafiles(self, args, projectId):

    ###
    # Get the MediaFiles

    mediaFileIds = []
    url = ARLO_API_V0_URL + "project/{}/mediaFiles/".format(projectId)

    while url is not None:

        if args.debug:
            print("DEBUG: Calling URL: " + url)
        resp = args.session.get(url, verify=VERIFY_SSL)
        if resp.status_code != requests.codes.ok:
            print("ERROR: Could not Retrieve Project MediaFiles: " + str(resp.status_code))
            sys.exit(1)

        # decode JSON
        mediaFiles = resp.json()

        for mf in mediaFiles['results']:
            mediaFileIds.append(mf['id'])

        url = mediaFiles['next']

    return mediaFileIds


  ###
  # Retrieve the PitchTrace for a full audio file from ARLO.
  #
  # Writes the data out to args.outfile
  #
  # @param args Namespace of the program's settings arguments (originally
  #             parsed from command line)
  # @param outfile Opened Output File object into which to write the data.
  # @param mediaFileId MediaFile ID to process
  # @param settingsDict Dictionary of settings passed on to the API
  # @return True on Success, False on any error


  def process_mediafile(self, args, outfile, mediaFileId, settingsDict):

    ###
    # get the file duration

    mediafile_duration = self.get_mediafile_duration(args, mediaFileId)
    if args.debug:
      print("DEBUG: MediaFile Duration: " + str(mediafile_duration) + "s")


    ###
    # start/end Times

    startTime = args.startTime
    endTime = args.endTime

    if startTime is None:
      startTime = 0

    if startTime < 0:
      print("ERROR: startTime < 0")
      sys.exit(1)

    if startTime > mediafile_duration:
      print("ERROR: startTime > file duration")
      sys.exit(1)

    if endTime is None:
      endTime = mediafile_duration

    if endTime < 0:
      print("ERROR: endTime < 0")
      sys.exit(1)

    if endTime > mediafile_duration:
      if args.debug:
        print("WARNING: endTime > file duration - resetting")
      endTime = mediafile_duration

    if args.debug:
      print("DEBUG: File startTime: " + str(startTime) + "  endTime: " + str(endTime))


    ###
    # Compute max window size to retrieve

    max_window_size = 100000

    ###
    # iterate over file, retrieving each window

    url = ARLO_API_V0_URL + "getAudioPitchTraceData/" + str(mediaFileId) + "/"
    if args.debug:
      print("DEBUG: URL: " + url)

    params = settingsDict
    window_start = startTime
    window_end = window_start + max_window_size

    while window_start < endTime:
      if window_end > endTime:
        window_end = endTime

      params['startTime'] = window_start
      params['endTime'] = window_end

      if args.debug:
        print("DEBUG: Retrieving Window " + str(window_start) + " - " + str(window_end))

      ###
      # Request Data

      resp = args.session.get(url, params=params, verify=VERIFY_SSL)

      if resp.status_code != requests.codes.ok:
        print("ERROR: Failed Retrieving MediaFile PitchTrace: Status " + str(resp.status_code) + " - " + str(resp.text))
        sys.exit(1)

      ###
      # decode JSON

      respData = resp.json()

      ###
      # Write to outfile

      # Generate Header
      settings_keys = [
                    'pitchTraceType',
                    'numTimeFramesPerSecond',
                    'numFrequencyBands',
                    'dampingFactor',
                    'spectraMinFrequency',
                    'spectraMaxFrequency',
                    ]
      if settingsDict['pitchTraceType'] == 'FUNDAMENTAL':
        settings_keys.extend([
                    'numSamplePoints',
                    'minCorrelation',
                    'entropyThreshold',
                    'pitchTraceStartFreq',
                    'pitchTraceEndFreq',
                    'minEnergyThreshold',
                    'tolerance',
                    'inverseFreqWeight',
                    'maxPathLengthPerTransition',
                    'windowSize',
                    'extendRangeFactor',
                  ])

      headerline = "fileTime,frequency,energy,"
      for k in settings_keys:
        headerline = headerline + ",{}: {}".format(k, settingsDict[k])

      outfile.write(headerline + "\n")

      frames = sorted(respData['pitchTraceData']['frames'].keys(), key=float)
      for entry in frames:
        outfile.write("{},{},{}\n".format(entry, respData['pitchTraceData']['frames'][entry]['frequency'], respData['pitchTraceData']['frames'][entry]['energy']))

      window_start = window_end
      window_end += max_window_size


    return True


class MaxEnergyPitchTrace(PitchTrace):

  def go(self, args):
    settings = {
      'pitchTraceType': 'MAX_ENERGY',
      'numTimeFramesPerSecond': args.numTimeFramesPerSecond,
      'numFrequencyBands': args.numFrequencyBands,
      'dampingFactor': args.dampingFactor,
      'spectraMinFrequency': args.minFrequency,
      'spectraMaxFrequency': args.maxFrequency,
      }

    mediaFileIds = []
    if args.project:
      mediaFileIds = self.get_project_mediafiles(args, args.project)
    else:
      mediaFileIds.append(args.mediafile)

    for mediaFileId in mediaFileIds:

      # Open File
      filename = 'PitchTrace-MaxEnergy-{}.csv'.format(mediaFileId)
      if os.path.isfile(filename):
        print("ERROR: File already exists - aborting: " + filename)
        return False
      with open(filename, 'w') as f:
        success =  self.process_mediafile(args, f, mediaFileId, settings)

    return success

class FundamentalPitchTrace(PitchTrace):

  def go(self, args):
    settings = {
      'pitchTraceType': 'FUNDAMENTAL',
      'numTimeFramesPerSecond': args.numTimeFramesPerSecond,
      'numFrequencyBands': args.numFrequencyBands,
      'dampingFactor': args.dampingFactor,
      'spectraMinFrequency': args.minFrequency,
      'spectraMaxFrequency': args.maxFrequency,

      'numSamplePoints': args.numSamplePoints,
      'minCorrelation': args.minCorrelation,
      'entropyThreshold': args.entropyThreshold,
      'pitchTraceStartFreq': args.pitchTraceStartFreq,
      'pitchTraceEndFreq': args.pitchTraceEndFreq,
      'minEnergyThreshold': args.minEnergyThreshold,
      'tolerance': args.tolerance,
      'inverseFreqWeight': args.inverseFreqWeight,
      'maxPathLengthPerTransition': args.maxPathLengthPerTransition,
      'windowSize': args.windowSize,
      'extendRangeFactor': args.extendRangeFactor,
      }

    mediaFileIds = []
    if args.project:
      mediaFileIds = self.get_project_mediafiles(args, args.project)
    else:
      mediaFileIds.append(args.mediafile)

    for mediaFileId in mediaFileIds:

      # Open File
      filename = 'PitchTrace-Fundamental-{}.csv'.format(mediaFileId)
      if os.path.isfile(filename):
        print("ERROR: File already exists - aborting: " + filename)
        return False
      with open(filename, 'w') as f:
        if not self.process_mediafile(args, f, mediaFileId, settings):
          return False

    return True



def main(argv):

  ###
  # Get Command Line Arguments

  parser = argparse.ArgumentParser()
  parser.add_argument('-d', '--debug', help="Show Debug Output", default=False, action='store_true')

  parser.add_argument('-u', '--user', dest='user', help="Username of the Login User")
  parser.add_argument('-p', '--password', '--pass', dest='password', help="Password of the Login User")

  # MediaFile Params
  parser.add_argument('--mediafile', help="MediaFile Id", required=False)
  parser.add_argument('--project', help="Project Id", required=False)
  parser.add_argument('--startTime', help="Override Start Time", required=False, type=float)
  parser.add_argument('--endTime', help="Override End Time", required=False, type=float)

  # Spectra Params
  parser.add_argument('--numTimeFramesPerSecond', help="Number of Time Frames per Second", required=True, type=int)
  parser.add_argument('--numFrequencyBands', help="Number of Frequency Bands", required=True, type=int)
  parser.add_argument('--dampingFactor', help="Damping Factor", required=True, type=float)
  parser.add_argument('--minFrequency', help="Minimum Frequency Band", required=True, type=int)
  parser.add_argument('--maxFrequency', help="Maximum Frequency Band", required=True, type=int)

  # PitchTrace Params
  parser.add_argument('--pitchTraceType', help="'MAX_ENERGY' / 'FUNDAMENTAL'", choices=['MAX_ENERGY', 'FUNDAMENTAL'], required=True)
  parser.add_argument('--numSamplePoints', required=False, type=int)
  parser.add_argument('--minCorrelation', required=False, type=float)
  parser.add_argument('--entropyThreshold', required=False, type=float)
  parser.add_argument('--pitchTraceStartFreq', required=False, type=int)
  parser.add_argument('--pitchTraceEndFreq', required=False, type=int)
  parser.add_argument('--minEnergyThreshold', required=False, type=float)
  parser.add_argument('--tolerance', required=False, type=float)
  parser.add_argument('--inverseFreqWeight', required=False, type=float)
  parser.add_argument('--maxPathLengthPerTransition', required=False, type=float)
  parser.add_argument('--windowSize', required=False, type=int)
  parser.add_argument('--extendRangeFactor', required=False, type=float)


  args = parser.parse_args()

  if args.debug:
    print("DEBUG MODE ENABLED")
    print(args)

  pitchTracer = None
  if args.pitchTraceType == 'MAX_ENERGY':
    pitchTracer = MaxEnergyPitchTrace()
  elif args.pitchTraceType == 'FUNDAMENTAL':
    pitchTracer = FundamentalPitchTrace()
  else:
    print("Invalid PitchTrace Type")
    sys.exit(1)

  if not pitchTracer.login(args):
    print("Failed Login")
    sys.exit(1)

  if pitchTracer.go(args):
    if args.debug:
      print("\nDEBUG: Successfully Completed")
  else:
    print("Some Errors encountered During Processing - Check Output!!!")
    sys.exit(1)


if __name__ == "__main__":
   main(sys.argv[1:])


