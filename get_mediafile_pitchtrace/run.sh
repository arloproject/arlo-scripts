PROG="python get_mediafile_pitchtrace.py "

PROJECT_ID="310"

# Settings
USER="tonyb"

START_TIME="0"
END_TIME="60"

NUM_TIME_FRAMES_PER_SECOND="200"
NUM_FREQ_BANDS="1024"
DAMPING_FACTOR="0.01"
MIN_FREQ="75"
MAX_FREQ="10000"

PITCHTRACE_TYPE="FUNDAMENTAL"
NUM_SAMPLE_POINTS="1024"
MIN_CORRELATION="0.02"
ENTROPY_THRESHOLD="999999"
PITCH_START_FREQ="100"
PITCH_END_FREQ="350"
MIN_ENERGY="0.05"
TOLERANCE="0.1"
INVERSE_FREQ_WEIGHT="0.5"
MAX_PATH_LENGTH="4"
WINDOW_SIZE="13"
EXTEND_RANGE_FACTOR="2"

$PROG \
  --user $USER \
  --startTime $START_TIME \
  --endTime $END_TIME \
  --numTimeFramesPerSecond $NUM_TIME_FRAMES_PER_SECOND \
  --numFrequencyBands $NUM_FREQ_BANDS \
  --dampingFactor $DAMPING_FACTOR \
  --minFrequency $MIN_FREQ \
  --maxFrequency $MAX_FREQ \
  --pitchTraceType $PITCHTRACE_TYPE \
  --minEnergyThreshold $MIN_ENERGY \
  --numSamplePoints $NUM_SAMPLE_POINTS \
  --windowSize $WINDOW_SIZE \
  --extendRangeFactor $EXTEND_RANGE_FACTOR \
  --maxPathLengthPerTransition $MAX_PATH_LENGTH \
  --entropyThreshold $ENTROPY_THRESHOLD \
  --pitchTraceStartFreq $PITCH_START_FREQ \
  --pitchTraceEndFreq $PITCH_END_FREQ \
  --inverseFreqWeight $INVERSE_FREQ_WEIGHT \
  --tolerance $TOLERANCE \
  --minCorrelation $MIN_CORRELATION \
  --project $PROJECT_ID

