get_mediafile_spectra.py
============

get\_mediafile\_spectra.py retrieves raw spectra data for an audio file from 
ARLO and saves it so a CSV formatted file. 

    usage: get_mediafile_spectra.py [-h] [-d] [-u USER] [-p PASSWORD] --mediafile
                                    MEDIAFILE --numTimeFramesPerSecond
                                    NUMTIMEFRAMESPERSECOND --numFrequencyBands
                                    NUMFREQUENCYBANDS --dampingFactor
                                    DAMPINGFACTOR --minFrequency MINFREQUENCY
                                    --maxFrequency MAXFREQUENCY
                                    [--startTime STARTTIME] [--endTime ENDTIME]
                                    [-o OUTFILE]

    optional arguments:
      -h, --help            show this help message and exit
      -d, --debug           Show Debug Output
      -u USER, --user USER  Username of the Login User
      -p PASSWORD, --password PASSWORD, --pass PASSWORD
                            Password of the Login User
      --mediafile MEDIAFILE
                            MediaFile Id
      --numTimeFramesPerSecond NUMTIMEFRAMESPERSECOND
                            Number of Time Frames per Second
      --numFrequencyBands NUMFREQUENCYBANDS
                            Number of Frequency Bands
      --dampingFactor DAMPINGFACTOR
                            Damping Factor
      --minFrequency MINFREQUENCY
                            Minimum Frequency Band
      --maxFrequency MAXFREQUENCY
                            Maximum Frequency Band
      --startTime STARTTIME
                            Override Start Time
      --endTime ENDTIME     Override End Time
      -o OUTFILE, --outfile OUTFILE
                            Output File (STDOUT by default)



### Output

This script generates a CSV formatted file, with Frequency Bands as columns, 
and a row for each computed frame, where the number of rows will be equal to 
NUMTIMEFRAMESPERSECOND times the duration of the file. The file contents 
consist of:

* A header row starting with the 'time' column, followed by a column for each Frequency Band (in ascending order)
* A row for each frame computed in the file, starting with the 'time' column, followed by one column for each frequency band.

Example, 5 bands from 20Hz to 20KHz: 

    time,20.0,112.468265038,632.455532034,3556.55882008,20000.0
    220.0,47064,668280,489747,261917,14128
    220.2,90581,1588436,509355,230240,9245


### Usage Example

Interactive Use

    $ python get_mediafile_spectra.py --mediafile 179070 --numTimeFramesPerSecond 128 --numFrequencyBands 128 --dampingFactor 0.02 --minFrequency 20 --maxFrequency 20000

Scripted Use

    $ python get_mediafile_spectra.py -u tonyb -p <PASSWORD> --mediafile 179070 --numTimeFramesPerSecond 128 --numFrequencyBands 128 --dampingFactor 0.02 --minFrequency 20 --maxFrequency 20000 -o outfile.csv

### Required Modules

Requirements are maintained in requirements.txt. 

To install with pip: 
`pip install -r requirements.txt`

### Notes and Known Issues

Currently the AGC seems to be active in spectra generation (in ARLO Adapt). 
Since we are windowing these requests, this means adjacent windows may be 
scaled differently vertically. 

### License

The MIT License (MIT)

Copyright (c) 2015 tonyborries

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

