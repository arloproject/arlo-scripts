#!/usr/bin/python

# The MIT License (MIT)
# 
# Copyright (c) 2015 tonyborries
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
import argparse
import getpass
import requests
import re


# raw_input in 2.x, input in 3.x
try: input = raw_input
except NameError: pass


#########
# Settings

ARLO_BASE_URL = "http://live.arloproject.com:81/"  # must have trailing slash

ARLO_LOGIN_URL  = ARLO_BASE_URL + "accounts/login/?next=/tools/userHome"
ARLO_API_V0_URL = ARLO_BASE_URL + "api/v0/"


###
# Login to ARLO. 
# 
# Save a logged-in requests.Session (session) object and the CSRF 
# token (csrf_token) needed for form POSTs.
#
# @param args Namespace of the program's settings arguments (originally 
#             parsed from command line)
# @return True on Success, False on any error

def login(args):

  ###
  # Get Credentials

  if args.user is None:
    args.user = input("Username: ")
  if args.password is None:
    args.password = getpass.getpass("Password: ")

  if args.debug:
    print("DEBUG: Logging in to ARLO...")

  ###
  # Setup Session

  args.session = requests.Session()
  args.session.headers['User-Agent'] = 'Get_MediaFile_Spectra/0.1'

  ###
  # Get CSRF Token

  if args.debug:
    print("DEBUG: Retrieving Login Form...")

  resp = args.session.get(ARLO_LOGIN_URL)
  if resp.status_code != requests.codes.ok:
    print("ERROR: Could not retrieve Login Form: " + str(resp.status_code))
    sys.exit(1)


  match = re.search("name='csrfmiddlewaretoken' value='([^']*)", resp.text);
  if match is None:
    print("ERROR: Could not retrieve CSRF Token")
    sys.exit(1)
  args.csrf_token = match.group(1)

  if args.debug:
    print("DEBUG: Found CSRF Token: " + args.csrf_token)
  
  ###
  # Login

  if args.debug:
    print("DEBUG: Login POST...")

  resp = args.session.post(ARLO_LOGIN_URL, {
              'csrfmiddlewaretoken': args.csrf_token, 
              'username': args.user, 
              'password': args.password,
              })
  if resp.status_code != requests.codes.ok:
    print("ERROR: Could not Login: " + str(resp.status_code))
    sys.exit(1)

  # validate the login by checking for the Welcome string
  if re.search("Welcome, <strong>", resp.text) is None:
    print("ERROR: Failed Logging In - Check user/password")
    sys.exit(1)

  if args.debug:
    print("DEBUG: Successfully Logged In!")

  args.password = "Password Cleared From Memory"

  return True


### Lookup the duration (in seconds) of the MediaFile.
# 
# @param args Namespace of the program's settings arguments (originally 
#             parsed from command line)
# @return Length of the MediaFile, in seconds. 

def get_mediafile_duration(args):

  ###
  # Get the MediaFile Details

  url = ARLO_API_V0_URL + "mediaFile/" + str(args.mediafile) + "/"
  if args.debug:
    print("DEBUG: Calling URL: " + url)
  resp = args.session.get(url)
  if resp.status_code != requests.codes.ok:
    print("ERROR: Could not Retrieve MediaFile Details: " + str(resp.status_code))
    sys.exit(1)

  # decode JSON
  mediafile_detail = resp.json()

  ###
  # find the duration in the metadata

  if args.debug:
    print("DEBUG: Searching MediaFile MetaData for durationInSeconds")

  mediafile_duration = None
  for datum in mediafile_detail['mediafilemetadata_set']:
    if args.debug:
      print ("DEBUG:   - " + datum['name'] + ": " + datum['value'])
    if datum['name'] == "durationInSeconds":
      mediafile_duration = float(datum['value'])

  if mediafile_duration is None:
    print("ERROR: Failed Retrieving MediaFile Duration")
    sys.exit(1)

  return mediafile_duration


###
# Retrieve the spectra for a full audio file from ARLO. 
# 
# Writes the data out to args.outfile
#
# @param args Namespace of the program's settings arguments (originally 
#             parsed from command line)
# @return True on Success, False on any error

def process_mediafile_spectra(args):

  ###
  # get the file duration

  mediafile_duration = get_mediafile_duration(args)
  if args.debug:
    print("DEBUG: MediaFile Duration: " + str(mediafile_duration) + "s")


  ###
  # start/end Times

  if args.startTime is None:
    args.startTime = 0

  if (args.startTime < 0):
    print("ERROR: startTime < 0")
    sys.exit(1)

  if (args.startTime > mediafile_duration): 
    print("ERROR: startTime > file duration")
    sys.exit(1)

  if args.endTime is None:
    args.endTime = mediafile_duration

  if (args.endTime < 0):
    print("ERROR: endTime < 0")
    sys.exit(1)

  if (args.endTime > mediafile_duration):
    print("ERROR: endTime > file duration")
    sys.exit(1)
  
  if args.debug:
    print("DEBUG: File startTime: " + str(args.startTime) + "  endTime: " + str(args.endTime))


  ###
  # Compute max window size to retrieve

  max_window_size = 60

  ### 
  # iterate over file, retrieving each window

  spectra_url = ARLO_API_V0_URL + "getAudioSpectraData/" + str(args.mediafile) + "/"
  if args.debug:
    print("DEBUG: Spectra URL: " + spectra_url)

  freq_bands = None

  window_start = args.startTime
  window_end = window_start + max_window_size

  while window_start < args.endTime:
    if window_end > args.endTime:
      window_end = args.endTime

    if args.debug: 
      print("DEBUG: Retrieving Window " + str(window_start) + " - " + str(window_end))

    ###
    # Request Data

    resp = args.session.get(spectra_url, params = {
            'startTime': window_start,
            'endTime': window_end,
            'numTimeFramesPerSecond': args.numTimeFramesPerSecond,
            'numFrequencyBands': args.numFrequencyBands,
            'dampingFactor': args.dampingFactor,
            'minFrequency': args.minFrequency,
            'maxFrequency': args.maxFrequency,
            })

    if resp.status_code != requests.codes.ok:
      print("ERROR: Failed Retrieving MediaFile Spectra: Status " + str(resp.status_code) + " - " + str(resp.text))
      sys.exit(1)

    ###
    # decode JSON

    spectra = resp.json()


    ###
    # Write to outfile

	# frequency bands

    if freq_bands is None: 
      # run the first time through
      # Numeric sort the frequency bands
      freq_bands = spectra['spectraData']['freqBands']
      args.outfile.write("time," + ",".join(str(x) for x in freq_bands) + "\n")

    # Add samples to Output File

    for frame_time in sorted(spectra['spectraData']['frames'].keys(), key=float):
      args.outfile.write(frame_time + "," + ",".join(str(x) for x in spectra['spectraData']['frames'][frame_time]) + "\n")

    window_start = window_end
    window_end += max_window_size


  return True


def main(argv):

  ###
  # Get Command Line Arguments

  parser = argparse.ArgumentParser()
  parser.add_argument('-d', '--debug', help="Show Debug Output", default=False, action='store_true')
  parser.add_argument('-u', '--user', dest='user', help="Username of the Login User")
  parser.add_argument('-p', '--password', '--pass', dest='password', help="Password of the Login User")
  parser.add_argument('--mediafile', help="MediaFile Id", required=True)
  parser.add_argument('--numTimeFramesPerSecond', help="Number of Time Frames per Second", required=True)
  parser.add_argument('--numFrequencyBands', help="Number of Frequency Bands", required=True)
  parser.add_argument('--dampingFactor', help="Damping Factor", required=True)
  parser.add_argument('--minFrequency', help="Minimum Frequency Band", required=True)
  parser.add_argument('--maxFrequency', help="Maximum Frequency Band", required=True)
  parser.add_argument('--startTime', help="Override Start Time", required=False, type=float)
  parser.add_argument('--endTime', help="Override End Time", required=False, type=float)

  parser.add_argument('-o', '--outfile', help="Output File (STDOUT by default)", type=argparse.FileType('w'), default=sys.stdout)

  args = parser.parse_args()

  if args.debug:
    print("DEBUG MODE ENABLED")
    print(args)

  if not login(args):
    print("Failed Login")
    sys.exit(1)

  if process_mediafile_spectra(args):
    if args.debug:
      print("\nDEBUG: Successfully Retrieved File")
  else:
    print("Some Errors encountered During Processing - Check Output!!!")
    sys.exit(1)
  

if __name__ == "__main__":
   main(sys.argv[1:])


