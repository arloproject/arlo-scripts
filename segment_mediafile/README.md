segment_mediafile.py
============

**NOTE:** The RandomWindowAndSegmentingPlugin available within ARLO may
is a partial replacement for this. This script has more configurability
or may serve as a reference for similar tools.

segment\_mediafile.py 'segments' a MediaFile, creating a number of tags 
across the duration of the MediaFile as specified by the 'length' and 'step' 
parameters. 

This is intended as a demonstration script for API functions and debugging. 
Use care as this can generate excessively large numbers of Tags and may 
place a large load on the backend servers. 


    usage: segment_mediafile.py [-h] [-v] [-d] [-u USER] [-p PASSWORD] [-f]
                                --project PROJECT --mediafile MEDIAFILE --tagset
                                TAGSET --tagclass TAGCLASS [--machinetagged]
                                [--usertagged] --length LENGTH --step STEP
    
    optional arguments:
      -h, --help            show this help message and exit
      -v, --verbose         Verbose Output
      -d, --debug           Debug Output
      -u USER, --user USER  Username of the Login User
      -p PASSWORD, --password PASSWORD, --pass PASSWORD
                            Password of the Login User
      -f, --force           Don't Prompt For Confirmation
      --project PROJECT     Project Id
      --mediafile MEDIAFILE
                            MediaFile Id
      --tagset TAGSET       TagSet Id
      --tagclass TAGCLASS   TagClass Id
      --machinetagged       Flag the Tags as machineTagged
      --usertagged          Flag the Tags as userTagged
      --length LENGTH       Length (seconds) of each Segment
      --step STEP           Size (seconds) to step forward for each segment


### Usage Example

Interactive Use

    $ python segment_mediafile.py --project 93 --mediafile 179070 --tagset 769 --tagclass 6849 --length 2 --step 2 --usertagged 

Scripted Use, add the -f force flag.

    $ python segment_mediafile.py -u tonyb -p <PASSWORD> --project 93 --mediafile 179070 --tagset 769 --tagclass 6849 --length 2 --step 2 --machinetagged -f

### Required Modules

Requirements are maintained in requirements.txt. 

To install with pip: 
`pip install -r requirements.txt`

### License

The MIT License (MIT)

Copyright (c) 2015 tonyborries

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

