#!/usr/bin/python


##############################################################################
#                                                                            #
#                                  WARNING                                   #
#                                 ---------                                  #
#                                                                            #
#                              Here be dragons.                              #
#                                                                            #
#  This script is intended to be used for demonstration/debugging purposes.  #
#  As this may generate a very large number of Tags, special consideration   #
#  should be taken to avoid overwhelming ARLO with a large amount of data    #
#  and taxing the system with requests. --TonyB                              #
#                                                                            #
#                          Proceed with caution!!!                           #
#                                                                            #
##############################################################################


# The MIT License (MIT)
# 
# Copyright (c) 2015 tonyborries
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import sys
import argparse
import getpass
import requests
import re
import json


# raw_input in 2.x, input in 3.x
try: input = raw_input
except NameError: pass


#########
# Settings

ARLO_BASE_URL = "http://live.arloproject.com:81/"  # must have trailing slash

ARLO_LOGIN_URL      = ARLO_BASE_URL + "accounts/login/?next=/tools/userHome"
ARLO_API_V0_URL     = ARLO_BASE_URL + "api/v0/"
ARLO_API_TOKEN_URL  = ARLO_BASE_URL + "api-token-auth/"

VERIFY_SSL = True  # Whether to Verify or Ignore TLS Cert errors


###
# Login to ARLO. 
# 
# Save a logged-in requests.Session (session) object and the CSRF 
# token (csrf_token) needed for form POSTs.
#
# @param args Namespace of the program's settings arguments (originally 
#             parsed from command line)
# @return True on Success, False on any error

def login(args):

  ###
  # Get Credentials

  if args.user is None:
    args.user = input("Username: ")
  if args.password is None:
    args.password = getpass.getpass("Password: ")

  if args.verbose:
    print("Logging in to ARLO...")



  ###
  # Setup an API Session For POSTs

  if args.debug:
    print("Retrieving API Token...")

  args.session = requests.Session()
  args.session.headers['User-Agent'] = 'Mass_Import_MediaFiles/0.1'
  args.session.headers['Referer'] = ARLO_BASE_URL
  args.session.headers['Content-Type'] = "application/json"

  resp = args.session.post(ARLO_API_TOKEN_URL, json.dumps({  # Note not using the Authenticated Session
                'username': args.user, 
                'password': args.password,
              }), 
              verify=VERIFY_SSL)

  if resp.status_code != requests.codes.ok:
    print("ERROR: Failed Retrieving API Token: " + str(resp.status_code))
    if args.debug:
      print(resp.text)
    sys.exit(1)

  resp_dict = json.loads(resp.text)
  api_token = resp_dict['token']
  
  if args.debug:
    print("Got API Token: " + api_token)

  args.session.headers['Authorization'] = "Token " + api_token

  if args.debug:
    print("API Session Headers:\n" + repr(args.session.headers))


  args.password = "Password Cleared From Memory"

  return True


### Lookup the duration (in seconds) of the MediaFile.
# 
# @param args Namespace of the program's settings arguments (originally 
#             parsed from command line)
# @return Length of the MediaFile, in seconds. 

def get_mediafile_duration(args):

  ###
  # Get the MediaFile Details

  url = ARLO_API_V0_URL + "mediaFile/" + str(args.mediafile) + "/"
  if args.debug:
    print("DEBUG: Calling URL: " + url)
  resp = args.session.get(url, verify=VERIFY_SSL)
  if resp.status_code != requests.codes.ok:
    print("ERROR: Could not Retrieve MediaFile Details: " + str(resp.status_code))
    sys.exit(1)

  # decode JSON
  mediafile_detail = resp.json()

  ###
  # find the duration in the metadata

  if args.debug:
    print("DEBUG: Searching MediaFile MetaData for durationInSeconds")

  mediafile_duration = None
  for datum in mediafile_detail['mediafilemetadata_set']:
    if args.debug:
      print ("DEBUG:   - " + datum['name'] + ": " + datum['value'])
    if datum['name'] == "durationInSeconds":
      mediafile_duration = float(datum['value'])

  if mediafile_duration is None:
    print("ERROR: Failed Retrieving MediaFile Duration")
    sys.exit(1)

  return mediafile_duration



###
# Segment a file. 
# 
# Create Tags of 'length' starting every 'step' for a MediaFile, up to it's duration. 
#
# @param args Namespace of the program's settings arguments (originally 
#             parsed from command line)
# @return True on Success, False on any error


def segment_file(args):

  projectId = args.project
  mediaFileId = args.mediafile
  tagSetId = args.tagset
  tagClassId = args.tagclass
  machineTagged = args.machinetagged
  userTagged = args.usertagged
  length = float(args.length)
  step = float(args.length)

  if (step <= 0): 
    print("ERROR: step <= 0")
    return False

  if (length <= 0): 
    print("ERROR: length <= 0")
    return False

  if not (machineTagged or userTagged):
    print("ERROR: One of machineTagged or userTagged must be specified.")
    return False

  # retrive the duration of the MediaFile
  fileDuration = get_mediafile_duration(args)

  if args.verbose:
    print("MediaFile Duration: " + str(fileDuration))


  ###
  # Generate dictionaries for each new Tag

  newTags = []

  startTime = 0
  while (startTime + length) < fileDuration:
    newTags.append({
        "tagSet": tagSetId,
        "mediaFile": mediaFileId,
        "startTime": startTime,
        "endTime": startTime + length,
        "minFrequency": 20,
        "maxFrequency": 20000,
        "tagClass": tagClassId,
        "machineTagged": machineTagged,
        "userTagged": userTagged,
        "strength": 1})
    startTime = startTime + step


  ### 
  # Confirm 

  if not args.force:
    print("\nGenerated " + str(len(newTags)) + " new Tags. Proceed to Save to ARLO?")
    trash = input("Press Enter To Continue (Ctrl-C to Abort)")


  ###
  # Send all Tags as one request.
  url = ARLO_API_V0_URL + "bulkTagCreate/" + str(args.project) + "/"

  if args.debug:
    print("POSTing to " + url)

  resp = args.session.post(url, data=json.dumps(newTags), verify=VERIFY_SSL, )

  if (resp.status_code < 200) or (resp.status_code > 299):
    print("ERROR: Failed Creating Tags - HTTP " + str(resp.status_code) + " - " + resp.text)
    sys.exit(1)

  if args.debug:
    print("Successful Response " + str(resp.status_code) + " - " + resp.text)

  return True


def main(argv):

  ###
  # Get Command Line Arguments

  parser = argparse.ArgumentParser()
  parser.add_argument('-v', '--verbose', help='Verbose Output', default=False, action='store_true')
  parser.add_argument('-d', '--debug', help="Debug Output", default=False, action='store_true')
  parser.add_argument('-u', '--user', dest='user', help="Username of the Login User")
  parser.add_argument('-p', '--password', '--pass', dest='password', help="Password of the Login User")
  parser.add_argument('-f', '--force', dest='force', help="Don't Prompt For Confirmation", action='store_true')
  parser.add_argument('--project', required=True, help="Project Id")
  parser.add_argument('--mediafile', required=True, help="MediaFile Id")
  parser.add_argument('--tagset', required=True, help="TagSet Id")
  parser.add_argument('--tagclass', required=True, help="TagClass Id")
  parser.add_argument('--machinetagged', default=False, action='store_true', help="Flag the Tags as machineTagged")
  parser.add_argument('--usertagged', default=False, action='store_true', help="Flag the Tags as userTagged")
  parser.add_argument('--length', required=True, help="Length (seconds) of each Segment")
  parser.add_argument('--step', required=True, help="Size (seconds) to step forward for each segment")

  args = parser.parse_args()

  if args.debug:
    args.verbose = True

  if args.debug:
    print("DEBUG MODE ENABLED")
    print(args)

  if not login(args):
    print("Failed Login")
    sys.exit(1)

  if segment_file(args):
    if args.verbose:
      print("\nSuccessfully Segmented File")
  else:
    print("Some Errors Encountered - Check Output!!!")
    sys.exit(1)

  

if __name__ == "__main__":
   main(sys.argv[1:])

